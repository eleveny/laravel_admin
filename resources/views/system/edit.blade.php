@extends('layout')
@section('title', $title)
@section('style')
@stop
@section('body')
    <blockquote class="layui-elem-quote">{{$title}}</blockquote>
    <form class="layui-form layui-form-pane ml5" onsubmit="return false;">
        <div class="layui-form-item">
            <label class="layui-form-label">用户名</label>
            <div class="layui-input-inline">
                <input type="text" name="name" required lay-verify="required" lay-verType="tips"
                       placeholder="请输入用户名"
                       autocomplete="off" class="layui-input" value="{{$user->name or ''}}">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">密码框</label>
            <div class="layui-input-inline">
                <input type="password" name="password" placeholder="请输入密码"
                       autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">确认密码</label>
            <div class="layui-input-inline">
                <input type="password" name="password2" placeholder="请输入密码"
                       autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <input type="button" class="layui-btn" lay-submit lay-filter="success" value="立即提交"/>
                <a href="javascript:window.history.back()" class="layui-btn layui-btn-primary">返回</a>
                <input type="hidden" value="{{$user->id or 0}}" name="id">
            </div>
        </div>
    </form>
@stop
@section('script')
    <script>
        $(function () {
            $(document).keyup(function (event) {
                if (event.keyCode == 13) {
                    $("#submit").trigger("click");
                }
            });
            layui.use(['form'], function () {
                var form = layui.form;
                form.on('submit(success)', function (data) {
                    var field = data.field;
                    $.post("{{route('system.edit')}}", field, function (rev) {
                        layer.alert(rev.msg, {skin: 'layui-layer-lan', btn: ['确认']}, function (index) {
                            console.log(typeof(rev.data));
                            if (rev.status === 0) {
                                window.location.href = rev.data;
                            }
                            layer.close(index);
                        }, function (index) {
                            layer.close(index);
                        });
                    });
                    return false;
                });
            });
            $("#reset").click(function () {
                $("input[name='password']").val('');
                $("input[name='password2']").val('');
            });
        });
    </script>
@stop