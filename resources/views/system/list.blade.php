@extends('layout')
@section('title', $title)
@section('style')
    <style>
        .ml5 {
            margin-left: 5px;
        }
    </style>
@stop
@section('body')
    <blockquote class="layui-elem-quote">{{$title}}</blockquote>
    <div class="layui-form">
        <div class="layui-form-item">
            <div class="layui-inline">
                <div class="layui-input-inline">
                    <input type="text" name="name" class="layui-input" placeholder="用户名">
                </div>
                <div class="layui-input-inline">
                    <button class="layui-btn" lay-submit lay-filter="success">搜索</button>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <a class="layui-btn" href="{{route('system.edit')}}">添加管理员</a>
    </div>
    <div class="layui-collapse">
        <div class="layui-colla-item ">
            <h2 class="layui-colla-title">帮助</h2>
            <div class="layui-colla-content">
                <ul>
                    <li><span class="layui-badge layui-bg-green mr5">1</span>用户管理员列表,可以搜索,可以删除,高级管理员不可以删除</li>
                </ul>
            </div>
        </div>
    </div>
    <table id="table-list" lay-filter="tableLay"></table>
    @verbatim
        <script type="text/html" id="isUseTpl">
            <input type="checkbox" name="" value="{{d.id}}" lay-skin="switch" lay-text="开启|禁止"
                   lay-filter="change" {{d.is_use== 1 ? 'checked' : '' }}>
        </script>
        <script type="text/html" id="barTable">
            <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
        </script>
    @endverbatim
@stop
@section('script')
    <script>
        layui.use(['table', 'form', 'element'], function () {
            var element = layui.element;
            var table = layui.table;
            var form = layui.form;
            loadList();

            function loadList(where) {
                if (typeof where === 'undefined') {
                    where = {};
                }
                table.render({
                    elem: '#table-list'
                    , height: 'auto'
                    , method: 'post'
                    , url: "{{route('system.list')}}" //数据接口
                    , where: where
                    , page: true //开启分页
                    , size: ''
                    , cellMinWidth: 60
                    , limit: 25
                    , limits: ['25', '50', '100']
                    , cols: [[ //表头
                        {field: 'sid', title: 'ID', sort: true, width: 80}
                        , {field: 'name', title: '订单号', width: 150, align: 'center', sort: true}
                        , {field: 'is_use', title: '帐号状态', width: 120, templet: '#isUseTpl'}
                        , {field: 'last_ip', title: '登陆IP', sort: true, width: 120}
                        , {field: 'last_format', title: '最后登陆时间', sort: true, width: 120}
                        , {field: 'created_format', title: '注册时间', sort: true, width: 150}
                        , {title: '操作', templet: '#barTable', width: 220}
                    ]]
                });
            }

            //操作
            table.on('tool(tableLay)', function (obj) {
                var data = obj.data;
                console.log(data);
                if (obj.event === 'del') {
                    var tips = '真的删除行么';
                    layer.confirm(tips, {icon: 2}, function (index) {
                        $.post("{{route('system.del')}}", {id: data.id}, function (rev) {
                            if (rev.status === 0) {
                                obj.del();
                            }
                            layer.msg(rev.msg);
                        });
                        layer.close(index);
                    });
                } else if (obj.event === 'edit') {
                    window.location.href = "{{route('system.edit')}}?id=" + data.id;
                }
            });
            //监听切换操作
            form.on('switch(change)', function (obj) {
                var pp = {};
                pp.id = this.value;
                pp.value = obj.elem.checked ? 1 : 0;
                $.post("{{route('system.change')}}", pp, function (rev) {
                    if (rev.status === 0) {
                        layer.msg(rev.msg);
                    } else {
                        layer.msg(rev.msg);
                    }
                }, 'json');
            });
            form.on('submit(success)', function (data) {
                var field = data.field;
                loadList(field);
            });
        });
    </script>
@stop